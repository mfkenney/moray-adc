#
# Makefile for MORAY ADC software.
#
BOARD = 24dsi

CC = gcc
CFLAGS = -Wall -O2 -I. \
	$(shell pkg-config --cflags glib-2.0 dbus-glib-1 uuid gs$(BOARD))

INSTALL = install
LIBTOOL = libtool

prefix = /usr
LIBDIR = $(DESTDIR)$(prefix)/lib
BINDIR = $(DESTDIR)$(prefix)/bin
DATADIR = $(DESTDIR)$(prefix)/share/vala
INCDIR = $(DESTDIR)$(prefix)/include
PCDIR = $(DESTDIR)$(prefix)/share/pkgconfig

VERS := "1.2.1"
# See section 7.3 of the Libtool manual for an explanation of the
# library version format.
LIB_VERS := "3:4:0"

ifeq ($(BOARD),24dsi16wrc)
VAR = wrc
lib_LTLIBRARIES := libmoray_adcwrc.la
libmoray_adcwrc_la_SOURCES := $(wildcard adc_*.c)
libmoray_adcwrc_la_OBJECTS := $(libmoray_adcwrc_la_SOURCES:.c=.lo)
libmoray_adcwrc_la_DEPLIBS := $(shell pkg-config --libs glib-2.0 dbus-glib-1 uuid) \
	-l$(BOARD)_utils -l$(BOARD)_dsl -lgsc_utils -lrt
libmoray_adcwrc_la_LDFLAGS := -version-info $(LIB_VERS) -rpath $(prefix)/lib \
	$(libmoray_adcwrc_la_DEPLIBS)
else
VAR =
lib_LTLIBRARIES := libmoray_adc.la
libmoray_adc_la_SOURCES := $(wildcard adc_*.c)
libmoray_adc_la_OBJECTS := $(libmoray_adc_la_SOURCES:.c=.lo)
libmoray_adc_la_DEPLIBS := $(shell pkg-config --libs glib-2.0 dbus-glib-1 uuid) \
	-l$(BOARD)_utils -l$(BOARD)_dsl -lgsc_utils -lrt
libmoray_adc_la_LDFLAGS := -version-info $(LIB_VERS) -rpath $(prefix)/lib \
	$(libmoray_adc_la_DEPLIBS)
endif

PROG := testadc
testadc_SOURCES := testadc.c
testadc_OBJECTS := testadc.o

HEADERS := moray_adc.h moray_adc_api.h
PKGCONFIG := moray_adc.pc

VALAC := valac
VAPI := moray_adc.vapi
VALA_CFLAGS = -v --thread --vapidir .
VALA_LDFLAGS := $(shell pkg-config --libs gobject-2.0 gthread-2.0)
PACKAGES = --pkg moray_adc --pkg posix --pkg stdio
#VERS := $(shell head -n 1 debian/changelog|sed -e 's/.*(\(.*\)).*/\1/')

all: $(lib_LTLIBRARIES)

prog: $(PROG)

ifeq ($(BOARD),24dsi16wrc)
$(libmoray_adcwrc_la_OBJECTS): %.lo: %.c
	$(LIBTOOL) --mode=compile $(CC) $(CFLAGS)  -c $<

libmoray_adcwrc.la: $(libmoray_adcwrc_la_OBJECTS)
	$(LIBTOOL) --mode=link $(CC) -o $@ $^ $(libmoray_adcwrc_la_LDFLAGS)

else
$(libmoray_adc_la_OBJECTS): %.lo: %.c
	$(LIBTOOL) --mode=compile $(CC) $(CFLAGS)  -c $<

libmoray_adc.la: $(libmoray_adc_la_OBJECTS)
	$(LIBTOOL) --mode=link $(CC) -o $@ $^ $(libmoray_adc_la_LDFLAGS)
endif

testlib.o: testlib.c
	$(CC) $(CFLAGS) -c $<

testlib: testlib.o $(lib_LTLIBRARIES)
	$(LIBTOOL) --mode=link $(CC) -o $@ $^

testadc.c: testadc.vala
	$(VALAC) $(VALA_CFLAGS) $(PACKAGES) --ccode $<

testadc: $(testadc_OBJECTS) $(lib_LTLIBRARIES) $(VAPI)
	$(LIBTOOL) --mode=link $(CC) -O2 $(VALA_LDFLAGS) -o testadc \
	$(testadc_OBJECTS) $(lib_LTLIBRARIES)

$(PKGCONFIG): $(PKGCONFIG).in
	sed -e "s/@VERSION@/$(VERS)/g" \
	    -e "s!@PREFIX@!$(prefix)!g" \
        -e "s!@REQ@!gs$(BOARD)!g" \
	    -e "s!@VAR@!$(VAR)!g" \
	  $(PKGCONFIG).in > $(PKGCONFIG)

install-lib: $(lib_LTLIBRARIES) $(PKGCONFIG)
	$(INSTALL) -d $(LIBDIR) $(INCDIR) $(DATADIR) $(PCDIR)
	$(LIBTOOL) --mode=install $(INSTALL) $(lib_LTLIBRARIES) $(LIBDIR)
	$(INSTALL) $(HEADERS) $(INCDIR)
	$(INSTALL) $(VAPI) $(DATADIR)
	$(INSTALL) $(PKGCONFIG) $(PCDIR)

install-bin: $(lib_LTLIBRARIES) $(PKGCONFIG) $(PROG)
	$(INSTALL) -d $(BINDIR)
	$(LIBTOOL) --mode=install $(INSTALL) -c $(PROG) $(BINDIR)/$(PROG)

install: install-lib

clean:
	rm -f *.o *.so *~ *.la *.lo $(PROG) testadc.c $(PKGCONFIG) testlib
	rm -rf .libs
