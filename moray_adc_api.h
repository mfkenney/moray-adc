/*
** Public API for the MORAY ADC library.
*/
#ifndef _MORAY_ADC_API_H_
#define _MORAY_ADC_API_H_

#ifndef ADC_NOINCLUDE
#include <stdint.h>
#endif

#ifndef ADC_API
#   if __GNUC__ >= 4
#       define ADC_API __attribute__((visibility("default")))
#   else
#       define ADC_API
#   endif
#endif

#define ADC_CHANNELS_PER_BOARD      12

/*
 * We are configuring the board for 24-bit 2's complement data values. The
 * following macros are used to extract the data value from the sample word.
 */
#define ADC_DATA_SIZE               24
#define ADC_DATA_MASK               ((1UL << ADC_DATA_SIZE) - 1)
#define ADC_DATA_SIGN_BIT           (1 << (ADC_DATA_SIZE-1))

typedef uint32_t adc_sample_t;

static __inline__ int32_t ADC_DATA_VALUE(adc_sample_t sample)
{
    return (sample & ADC_DATA_SIGN_BIT) ?
      (sample | ~ADC_DATA_MASK) :
      (sample & ADC_DATA_MASK);
}

typedef struct {
    adc_sample_t *master;
    adc_sample_t *slave;
} adc_scan_t;

struct adc_clock_s;
typedef struct adc_clock_s adc_clock_t;

struct ADC_s;
typedef struct ADC_s ADC_t;

typedef enum {
    TRIG_START=0,
    TRIG_END=1
} adc_trig_state_t;

typedef enum {
    BOARD_MASTER=0,
    BOARD_SLAVE=1
} adc_board_t;

typedef enum {
    INPUT_DIFF=0,
    INPUT_ZERO=1,
    INPUT_VREF=2
} adc_input_mode_t;


typedef void (*ScanFunc)(adc_scan_t *sp, unsigned m_chans, unsigned m_mask,
                         unsigned s_chans, unsigned s_mask, int n);
typedef void (*TrigFunc)(adc_trig_state_t state, char *token);

ADC_API ADC_t* adc_open(const char *master_dev, unsigned master_mask,
                        const char *slave_dev, unsigned slave_mask,
                        unsigned n_scans);
ADC_API unsigned adc_get_nchannels(ADC_t *adc, adc_board_t board);
ADC_API unsigned adc_get_mask(ADC_t *adc, adc_board_t board);
ADC_API int adc_channels_ready(ADC_t *adc, long timeout_ms);
ADC_API int adc_sync(ADC_t *adc);

ADC_API long adc_init(ADC_t *adc, long fsamp, long bufsize, long timeout, float v_peak);
ADC_API void adc_enable(ADC_t *adc);
ADC_API void adc_disable(ADC_t *adc);
ADC_API void adc_close(ADC_t *adc);
ADC_API int adc_arm(ADC_t *adc, long timeout_ms, int use_hardware);
ADC_API void adc_trigger(ADC_t *adc);
ADC_API long adc_read(ADC_t *adc, long n_samples, ScanFunc f, TrigFunc tf);
ADC_API long adc_read_now(ADC_t *adc, long n_samples, ScanFunc handler);
ADC_API void adc_vrange(ADC_t *adc, float v_peak);
ADC_API void adc_init_dbus_connection(void);
ADC_API void adc_input_mode(ADC_t *adc, adc_input_mode_t mode);

#endif /* _MORAY_ADC_API_H_ */
