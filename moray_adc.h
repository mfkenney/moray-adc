/*
**
*/
#ifndef _MORAY_ADC_H_
#define _MORAY_ADC_H_

#include "moray_adc_api.h"

#include <sys/ioctl.h>
#include <24dsi.h>
#include <errno.h>
#include <glib.h>
#include <semaphore.h>


#define sample_t        adc_sample_t
#define scan_t          adc_scan_t

/*
 * Channel group bit-masks
 */
#define GROUP_0                 ((1 << (ADC_CHANNELS_PER_BOARD/2)) - 1)
#define GROUP_1                 ((GROUP_0) << (ADC_CHANNELS_PER_BOARD/2))

/* Mask to extract the channel tag from each sample */
#define CHANNEL_TAG_MASK        0x1f000000

/* External trigger bit in Board Control register */
#define ARM_EXTERNAL_TRIGGER    (1L << 21)

/* Low Frequency Filter bit in Board Control register */
#define LOW_FREQ_FILTER         (1L << 19)

#define DATA_SIZE               ADC_DATA_SIZE
#define DATA_MASK               ADC_DATA_MASK
#define DATA_SIGN_BIT           ADC_DATA_SIGN_BIT
#define DATA_VALUE              ADC_DATA_VALUE

static __inline__ int DATA_WIDTH_CODE(void)
{
    switch(DATA_SIZE)
    {
        case 24:
            return DSI_DATA_WIDTH_24;
        case 20:
            return DSI_DATA_WIDTH_20;
        case 18:
            return DSI_DATA_WIDTH_18;
        default:
            return DSI_DATA_WIDTH_16;
    }
}

static __inline__ int AIN_RANGE_CODE(float voltage)
{
    float v = (voltage < 0.) ? -voltage : voltage;

    if(v > 5.)
        return DSI_AIN_RANGE_10V;
    if(v > 2.5)
        return DSI_AIN_RANGE_5V;
    return DSI_AIN_RANGE_2_5V;
}


struct adc_clock_s {
    int32_t fref;   /**> Reference oscillator frequency */
    int32_t fsamp;  /**> Sample frequency */
    int32_t nvco;   /**> PLL VCO factor */
    int32_t nref;   /**> PLL reference factor */
    int32_t nrate;  /**> Only used on legacy boards */
    int32_t ndiv;   /**> Rate divisor */
};

#define SCAN_BUF_LEN    20

struct ADC_s {
    int         master_fd;          /**> File descriptor for master board */
    int         slave_fd;           /**> File descriptor for slave board */
    unsigned    master_mask;        /**> Channels used on master */
    unsigned    slave_mask;         /**> Channels used on slave */
    unsigned    master_channels;
    unsigned    slave_channels;
    adc_clock_t clock;              /**> Clock settings */
    sem_t       triggered;          /**> Semaphore to signal a TRIGGER event */
    long        trigger_timeout;
    unsigned    n_scans;
    float       v_peak;
    scan_t      scan;
};

static __inline__ int _ioctl_set(int fd, int req, int val, const char *reqname)
{
    int32_t _val = val;
    int r;

    g_debug("ioctl(%d, %s)", fd, reqname);
    r = ioctl(fd, req, &_val);
    if(r < 0)
        g_warning("ioctl(%s) failed, errno = %d", reqname, errno);
    return r;
}

#define ioctl_set(fd, req, val) _ioctl_set(fd, req, val, #req)


#endif /* _MORAY_ADC_H_ */
