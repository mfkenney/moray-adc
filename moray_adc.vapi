/*
** Vala bindings for adc
*/

namespace Moray
{
    [CCode (cheader_filename = "24dsi.h", cname = "DSI_BASE_NAME")]
    public const string DSI_BASE_NAME;
    [CCode (cheader_filename = "moray_adc_api.h", cname = "ADC_DATA_SIZE")]
    public const int DATA_SIZE;
    [CCode (cheader_filename = "moray_adc_api.h", cname = "ADC_DATA_MASK")]
    public const int DATA_MASK;
    [CCode (cheader_filename = "moray_adc_api.h", cname = "ADC_DATA_SIGN_BIT")]
    public const int DATA_SIGN_BIT;
    [CCode (cheader_filename = "moray_adc_api.h", cname = "ADC_TRIG_START")]
    public const int TRIG_START;
    [CCode (cheader_filename = "moray_adc_api.h", cname = "ADC_TRIG_END")]
    public const int TRIG_END;

    public string devname(int board_num) {
        unowned string base = DSI_BASE_NAME;
        return @"$base$board_num";
    }

    public int32 DATA_VALUE(uint32 s) {
        return (int32)(((s & DATA_SIGN_BIT) == DATA_SIGN_BIT) ? (s | ~DATA_MASK) : (s & DATA_MASK));
    }

    [CCode (cheader_filename = "moray_adc_api.h", cname = "adc_scan_t")]
    public struct Scan {
        public uint32* master;
        public uint32* slave;
    }

    [CCode (cheader_filename = "moray_adc_api.h", cname = "ScanFunc", has_target = false)]
    public delegate void ScanFunc(Scan *sp, uint master_chans, uint master_mask,
                                  uint slave_chans, uint slave_mask, int n_scans);
    [CCode (cheader_filename = "moray_adc_api.h", cname = "TrigFunc", has_target = false)]
    public delegate void TrigFunc(int state, [CCode (array_length = false)] char[] token);

    [Compact]
    [CCode (cheader_filename = "moray_adc.h", cname = "ADC_t", cprefix = "adc_", free_function = "adc_close")]
    public class Adc {
        public uint master_mask;
        public uint slave_mask;
        [CCode (cname = "master_channels")]
        public uint master_channels_read;
        [CCode (cname = "slave_channels")]
        public uint slave_channels_read;
        [CCode (cname = "n_scans")]
        public uint scans_per_block;

        [CCode (cname = "adc_open")]
        public Adc(string? master_dev, uint master_mask, string? slave_dev, uint slave_mask, uint n_scans = 10);
        public long init(long fsamp, long bufsize, long timeout = 10, float v_peak = 5);
        public int sync();
        public void enable();
        public void disable();
        public int arm(long timeout_ms, bool use_hardware = false);
        public void trigger();
        public long read(long n_samples, ScanFunc handler, TrigFunc tf);
        public long read_now(long n_samples, ScanFunc handler);
    }
}
