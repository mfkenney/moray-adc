/*
**
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <fcntl.h>
#include <unistd.h>
#include <24dsi_utils.h>
#include <gsc_utils.h>
#include <glib.h>
#include <pthread.h>
#include <semaphore.h>
#include <errno.h>
#include <signal.h>
#include <dbus/dbus.h>
#include <uuid/uuid.h>
#include "moray_adc.h"

static DBusConnection *conn = NULL;
static pthread_t trigger_thread;
static pthread_mutex_t sync_mutex = PTHREAD_MUTEX_INITIALIZER;
static int intr_signals[] = {SIGHUP, SIGINT, SIGQUIT, SIGTERM};

#define NR_INTS (sizeof(intr_signals)/sizeof(int))

void
adc_init_dbus_connection(void)
{
    DBusError err;

    if(conn == NULL)
    {
        dbus_error_init(&err);
        conn = dbus_bus_get(DBUS_BUS_SYSTEM, &err);
        if(conn == NULL)
            g_critical("Cannot access DBus");
    }


}

static void
emit_dbus_signal(const char *contents)
{
    dbus_uint32_t serial;
    DBusMessage *msg;

    msg = dbus_message_new_signal("/org/moray/adc",
                                  "org.moray.adc",
                                  "trigger");
    if(contents && contents[0])
        dbus_message_append_args(msg,
                                 DBUS_TYPE_STRING, &contents,
                                 DBUS_TYPE_INVALID);

    dbus_message_set_no_reply(msg, TRUE);
    dbus_connection_send(conn, msg, &serial);
    dbus_message_unref(msg);
}

/*
 * Enable buffer overflow check
 */
static void
enable_buffer_check(ADC_t* adc)
{
    int32_t arg;

    arg = DSI_AIN_BUF_OVERFLOW_CLEAR;
    ioctl(adc->master_fd, DSI_IOCTL_AIN_BUF_OVERFLOW, &arg);
    if(adc->slave_fd)
        ioctl(adc->slave_fd, DSI_IOCTL_AIN_BUF_OVERFLOW, &arg);

    arg = DSI_IO_OVERFLOW_CHECK;
    ioctl(adc->master_fd, DSI_IOCTL_RX_IO_OVERFLOW, &arg);
    if(adc->slave_fd)
        ioctl(adc->slave_fd, DSI_IOCTL_RX_IO_OVERFLOW, &arg);
}

/*
 * Thread to wait for next SYNC.
 *
 */
static void*
wait_for_ready(void *thread_arg)
{
    ADC_t *adc = (ADC_t*)thread_arg;
    gsc_wait_t wait;
    int32_t save_irq, irq, sync_mode;
    sigset_t mask;
    int i;

    pthread_detach(pthread_self());

    /*
     * Insure that any interrupting signals are delivered to the main thread.
     */
    sigemptyset(&mask);
    for(i = 0;i < NR_INTS;i++)
        sigaddset(&mask, intr_signals[i]);

    pthread_sigmask(SIG_BLOCK, &mask, NULL);

    /* Clear buffers on next SYNC pulse */
    pthread_mutex_lock(&sync_mutex);
    sync_mode = DSI_SW_SYNC_MODE_CLR_BUF;
    ioctl(adc->master_fd, DSI_IOCTL_SW_SYNC_MODE, &sync_mode);
    if(adc->slave_fd)
        ioctl(adc->slave_fd, DSI_IOCTL_SW_SYNC_MODE, &sync_mode);
    pthread_mutex_unlock(&sync_mutex);

    /* Save current IRQ setting */
    save_irq = -1;
    ioctl(adc->master_fd, DSI_IOCTL_IRQ_SEL, &save_irq);
    irq = DSI_IRQ_CHAN_READY;
    ioctl(adc->master_fd, DSI_IOCTL_IRQ_SEL, &irq);

    g_debug("Waiting for CHANNEL_READY event");

    memset(&wait, 0, sizeof(wait));
    wait.gsc = DSI_WAIT_GSC_CHAN_READY;
    wait.timeout_ms = adc->trigger_timeout;
    ioctl(adc->master_fd, DSI_IOCTL_WAIT_EVENT, &wait);

    /* Restore IRQ setting */
    ioctl(adc->master_fd, DSI_IOCTL_IRQ_SEL, &save_irq);
    /* Restore normal SYNC mode */
    sync_mode = DSI_SW_SYNC_MODE_SW_SYNC;
    ioctl(adc->master_fd, DSI_IOCTL_SW_SYNC_MODE, &sync_mode);
    if(adc->slave_fd)
        ioctl(adc->slave_fd, DSI_IOCTL_SW_SYNC_MODE, &sync_mode);

    if(wait.flags == GSC_WAIT_FLAG_DONE)
        sem_post(&adc->triggered);
    else
        g_critical("CHANNEL_READY not detected");

    g_debug("Thread complete");

    return NULL;
}

/*
 * Thread to wait for next TRIGGER. This differs from the SYNC in that a TRIGGER
 * causes the buffer to be cleared automatically.
 *
 */
static void*
wait_for_trigger(void *thread_arg)
{
    ADC_t *adc = (ADC_t*)thread_arg;
    gsc_wait_t wait;
    int32_t save_irq, irq;
    sigset_t mask;
    int i;

    pthread_detach(pthread_self());

    /*
     * Insure that any interrupting signals are delivered to the main thread.
     */
    sigemptyset(&mask);
    for(i = 0;i < NR_INTS;i++)
        sigaddset(&mask, intr_signals[i]);

    pthread_sigmask(SIG_BLOCK, &mask, NULL);

    /* Save current IRQ setting */
    save_irq = -1;
    ioctl(adc->master_fd, DSI_IOCTL_IRQ_SEL, &save_irq);
    irq = DSI_IRQ_CHAN_READY;
    ioctl(adc->master_fd, DSI_IOCTL_IRQ_SEL, &irq);

    g_debug("Waiting for trigger generated CHANNEL_READY event");

    memset(&wait, 0, sizeof(wait));
    wait.gsc = DSI_WAIT_GSC_CHAN_READY;
    wait.timeout_ms = adc->trigger_timeout;
    ioctl(adc->master_fd, DSI_IOCTL_WAIT_EVENT, &wait);
    /* Restore IRQ setting */
    ioctl(adc->master_fd, DSI_IOCTL_IRQ_SEL, &save_irq);

    if(wait.flags == GSC_WAIT_FLAG_DONE)
        sem_post(&adc->triggered);
    else
        g_critical("CHANNEL_READY not detected");

    g_debug("Thread complete");

    return NULL;
}

int
adc_arm(ADC_t *adc, long timeout_ms, gboolean use_trigger)
{
    int err;
#ifdef TRIG_DEBUG
    u_int32_t reg_value;
#endif

    sem_init(&adc->triggered, 0, 0);
    adc->trigger_timeout = timeout_ms;

    if(use_trigger)
    {
        err = pthread_create(&trigger_thread, NULL, wait_for_trigger, adc);
    }
    else
        err = pthread_create(&trigger_thread, NULL, wait_for_ready, adc);

    if(err != 0)
    {
        g_critical("Cannot create thread (error code = %d)", err);
        sem_destroy(&adc->triggered);
        adc->trigger_timeout = -1;
        return -1;
    }

    if(use_trigger)
    {
        err = dsi_reg_mod(adc->master_fd, DSI_GSC_BCTLR,
                          ARM_EXTERNAL_TRIGGER, ARM_EXTERNAL_TRIGGER);
#ifdef TRIG_DEBUG
        if(err > 0)
            g_critical("Register write error, errno = %d", errno);

        dsi_reg_read(adc->master_fd, DSI_GSC_BCTLR, &reg_value);
        g_debug("Read back board-control register, BCR = 0x%08x", reg_value);
        if(!(reg_value & ARM_EXTERNAL_TRIGGER))
            g_critical("Trigger not set");
#endif
        if(adc->slave_fd)
        {
            dsi_reg_mod(adc->slave_fd, DSI_GSC_BCTLR,
                        ARM_EXTERNAL_TRIGGER, ARM_EXTERNAL_TRIGGER);
        }
    }

    return 0;
}

void
adc_trigger(ADC_t *adc)
{
    if(pthread_mutex_trylock(&sync_mutex) == 0)
    {
        g_debug("Sending software SYNC");
        ioctl(adc->master_fd, DSI_IOCTL_SW_SYNC, NULL);
        pthread_mutex_unlock(&sync_mutex);
        g_debug("SYNC sent");
    }
}


static void
catch_signal(int signum)
{
    g_message("Trigger wait interrupted");
    pthread_cancel(trigger_thread);
}

long
adc_read(ADC_t *adc, long n_samples, ScanFunc handler, TrigFunc tf)
{
    int master_bytes, slave_bytes, i, n_scans;
    long n;
    scan_t *sp;
    sample_t *buf;
    struct timespec ts;
    struct sigaction sa, old_sa[NR_INTS];
    uuid_t uuid;
    char uuid_buf[40];

    if(adc->trigger_timeout <= 0)
        return -1;

    clock_gettime(CLOCK_REALTIME, &ts);
    ts.tv_sec += adc->trigger_timeout/1000;
    ts.tv_nsec += ((adc->trigger_timeout % 1000)*1000000);

    master_bytes = adc->master_channels*sizeof(sample_t);
    slave_bytes = adc->slave_channels*sizeof(sample_t);

    g_debug("Bytes per scan: %d/%d", master_bytes, slave_bytes);

    n = n_samples;
    sp = &adc->scan;
    uuid_generate_time(uuid);
    uuid_unparse(uuid, uuid_buf);

    /*
     * Install a handler for the signals which can interrupt the wait.
     */
    sa.sa_handler = catch_signal;
    sigemptyset(&sa.sa_mask);
    sa.sa_flags = 0;
    for(i = 0;i < NR_INTS;i++)
        sigaction(intr_signals[i], &sa, &old_sa[i]);

    if(sem_timedwait(&adc->triggered, &ts) == 0)
    {
        if(conn)
            emit_dbus_signal(uuid_buf);
        if(tf)
            tf(TRIG_START, uuid_buf);

        enable_buffer_check(adc);

        /* We don't want adc_trigger called during a read */
        pthread_mutex_lock(&sync_mutex);
        g_debug("Reading data");
        while(n)
        {
            n_scans = (n > adc->n_scans) ? adc->n_scans : n;

            if(master_bytes)
            {
                buf = sp->master;
                if(read(adc->master_fd, buf, master_bytes*n_scans) < 0)
                {
                    g_critical("Read error on master board (n = %ld, errno = %d)", n, errno);
                    break;
                }
            }

            if(slave_bytes)
            {
                buf = sp->slave;
                if(read(adc->slave_fd, buf, slave_bytes*n_scans) < 0)
                {
                    g_critical("Read error on slave board (n = %ld, errno = %d)", n, errno);
                    break;
                }
            }

            if(handler)
                handler(sp,
                        adc->master_channels, adc->master_mask,
                        adc->slave_channels, adc->slave_mask, n_scans);
            n -= n_scans;
        }
        pthread_mutex_unlock(&sync_mutex);

        if(tf)
            tf(TRIG_END, NULL);

    }
    else
        g_critical("Timeout waiting for trigger");

    for(i = 0;i < NR_INTS;i++)
        sigaction(intr_signals[i], &old_sa[i], NULL);

    sem_destroy(&adc->triggered);

    ioctl_set(adc->master_fd, DSI_IOCTL_RX_IO_OVERFLOW, DSI_IO_OVERFLOW_IGNORE);
    ioctl_set(adc->master_fd, DSI_IOCTL_AIN_BUF_OVERFLOW, DSI_AIN_BUF_OVERFLOW_CLEAR);
    if(adc->slave_fd)
    {
        ioctl_set(adc->slave_fd, DSI_IOCTL_AIN_BUF_OVERFLOW, DSI_AIN_BUF_OVERFLOW_CLEAR);
        ioctl_set(adc->slave_fd, DSI_IOCTL_RX_IO_OVERFLOW, DSI_IO_OVERFLOW_IGNORE);
    }


    return (n_samples - n);
}

long
adc_read_now(ADC_t *adc, long n_samples, ScanFunc handler)
{
    int master_bytes, slave_bytes, n_scans;
    long n;
    scan_t *sp;
    sample_t *buf;

    master_bytes = adc->master_channels*sizeof(sample_t);
    slave_bytes = adc->slave_channels*sizeof(sample_t);

    g_debug("Bytes per scan: %d/%d", master_bytes, slave_bytes);

    n = n_samples;
    sp = &adc->scan;

    while(n)
    {
        n_scans = (n > adc->n_scans) ? adc->n_scans : n;

        if(master_bytes)
        {
            buf = sp->master;
            if(read(adc->master_fd, buf, master_bytes*n_scans) < 0)
            {
                g_critical("Read error on master board (errno = %d)", errno);
                break;
            }
        }

        if(slave_bytes)
        {
            buf = sp->slave;
            if(read(adc->slave_fd, buf, slave_bytes*n_scans) < 0)
            {
                g_critical("Read error on slave board (errno = %d)", errno);
                break;
            }
        }

        if(handler)
            handler(sp,
                    adc->master_channels, adc->master_mask,
                    adc->slave_channels, adc->slave_mask, n_scans);
        n -= n_scans;
    }

    return (n_samples - n);
}
