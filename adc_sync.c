/*
** A/D board synchronization functions.
*/
#include <unistd.h>
#include <24dsi_utils.h>
#include <gsc_utils.h>
#include <glib.h>
#include "moray_adc.h"

/* timeout in milliseconds */
#define CHANNELS_READY_TIMEOUT  10000
#define CHANNELS_READY          (1 << 13)

static int
_channels_ready(int fd, long timeout_ms)
{
    long timeout;
    int errs;
    u_int32_t bcr;

    if(fd <= 0)
        return 0;

    timeout = timeout_ms;
    do
    {
        errs = dsi_reg_read(fd, DSI_GSC_BCTLR, &bcr);
        if(errs)
            return -1;
        timeout--;
        usleep(1000);
    } while(!(bcr & CHANNELS_READY) && timeout > 0);

    if(timeout <= 0)
        g_critical("Channel ready timeout (fd = %d)", fd);

    return (timeout <= 0) ? -1 : 0;
}

int
adc_channels_ready(ADC_t *adc, long timeout_ms)
{
    return (_channels_ready(adc->master_fd, timeout_ms) < 0 ||
            _channels_ready(adc->slave_fd, timeout_ms) < 0) ? -1 : 0;
}

int
adc_sync(ADC_t *adc)
{
    ioctl_set(adc->master_fd, DSI_IOCTL_SW_SYNC_MODE, DSI_SW_SYNC_MODE_SW_SYNC);
    if(adc->slave_fd)
        ioctl_set(adc->slave_fd, DSI_IOCTL_SW_SYNC_MODE, DSI_SW_SYNC_MODE_SW_SYNC);
    ioctl(adc->master_fd, DSI_IOCTL_SW_SYNC, NULL);
    if(adc_channels_ready(adc, CHANNELS_READY_TIMEOUT) < 0)
        return -1;

    ioctl_set(adc->master_fd, DSI_IOCTL_SW_SYNC_MODE, DSI_SW_SYNC_MODE_CLR_BUF);
    if(adc->slave_fd)
        ioctl_set(adc->slave_fd, DSI_IOCTL_SW_SYNC_MODE, DSI_SW_SYNC_MODE_CLR_BUF);
    ioctl(adc->master_fd, DSI_IOCTL_SW_SYNC, NULL);
    if(adc_channels_ready(adc, CHANNELS_READY_TIMEOUT) < 0)
        return -1;

    ioctl_set(adc->master_fd, DSI_IOCTL_SW_SYNC_MODE, DSI_SW_SYNC_MODE_SW_SYNC);
    if(adc->slave_fd)
        ioctl_set(adc->slave_fd, DSI_IOCTL_SW_SYNC_MODE, DSI_SW_SYNC_MODE_SW_SYNC);

    return 0;
}
