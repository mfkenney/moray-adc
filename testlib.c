#include <stdio.h>
#include "moray_adc_api.h"


static void show_scan(adc_scan_t *sp, unsigned mchans, unsigned mmask,
                      unsigned schans, unsigned smask, int n_scans)
{
    int         i, j;
    unsigned    mask;
    uint32_t    *m_buf;

    m_buf = sp->master;

    for(j = 0; j < n_scans; j++)
    {
        for(i = 0,mask = 1; i < mchans; i++,m_buf++,mask <<= 1)
        {
            if((mask & mmask) == mask)
                printf("%d ", ADC_DATA_VALUE(*m_buf));

        }
        printf("\n");
    }
}

int main(int ac, char **av)
{
    ADC_t   *adc;

    adc = adc_open("/dev/24dsi.0", 0xff, NULL, 0, 10);
    if(adc != NULL)
    {
        adc_init(adc, 20000, 64, 10, 10.);
        adc_input_mode(adc, INPUT_ZERO);
        adc_arm(adc, 10000, 0);
        adc_trigger(adc);
        adc_read(adc, 10, show_scan, NULL);
        adc_input_mode(adc, INPUT_DIFF);
        adc_arm(adc, 10000, 0);
        adc_trigger(adc);
        adc_read(adc, 10, show_scan, NULL);
        adc_close(adc);
    }
    else
        return 1;

    return 0;
}