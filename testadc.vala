/*
** Simple ADC read test.
*/

class Trigger {
	unowned Moray.Adc	adc;
	uint delay_usec;
	int interval_usec;
	uint count;
	
	public Trigger(Moray.Adc adc, double interval, uint count, uint delay=2) {
		double t = interval*1000000;
		
		this.adc = adc;
		this.delay_usec = delay*1000000;
		this.interval_usec = (int)t;
		this.count = count;
	}

	public void* run() {
		uint t = this.delay_usec;
		Thread.usleep(t);
		t = this.interval_usec;
		
		while(true)
		{
			this.adc.trigger();
			Thread.usleep(t);
		}
		
	}
}

public class Testadc {
	static bool verbose;
	static uint max_level = LogLevelFlags.LEVEL_INFO;
	static unowned FileStream out_stream;
	static Stdio.BinaryStream data_stream;
	static string data_file = null;
	static uint n_master_channels = 8;
	static uint n_slave_channels = 4;
	static uint trig_count = 1;
	static uint scans_per_block = 10;
	static int pre_acq_delay = 0;
	static int gain = 0;
	static double trig_interval = 2;
	static double v_range = 5;
	static double delay = 0;
	
	const OptionEntry[] options = {
		{ "verbose", 'v', 0, OptionArg.NONE, ref verbose, "Print more diagnostic info to stderr", null},
		{ "master", 'm', 0, OptionArg.INT, out n_master_channels, 
		  "Number of channels to read on Master (board 0)", 
		  null},
		{ "slave", 's', 0, OptionArg.INT, out n_slave_channels, 
		  "Number of channels to read on Slave (board 1)", 
		  null},
		{ "count", 'c', 0, OptionArg.INT, out trig_count, "Number of triggers", null},
		{ "gain", 'g', 0, OptionArg.INT, out gain, "Input gain level in dB", null},
		{ "interval", 'i', 0, OptionArg.DOUBLE, out trig_interval, "Trigger interval in seconds", null},
		{ "range", 'r', 0, OptionArg.DOUBLE, out v_range, "Input voltage range: 2.5, 5, or 10", null},
		{ "delay", 'd', 0, OptionArg.DOUBLE, out delay, "Pre-acquisition delay in seconds", null},
		{ "binary", 'b', 0, OptionArg.FILENAME, ref data_file, "Specify a binary output file", null},
		{ "block_size", 0, 0, OptionArg.INT, out scans_per_block, 
		  "Number of scans per data block (buffer)", null},
		{ null }
        };

	static string get_timestamp() {
		Posix.timeval tv = Posix.timeval();
		tv.get_time_of_day();
		return "%ld.%06ld".printf(tv.tv_sec, tv.tv_usec);
	}
	
	static void start_sequence(uint fsamp, uint data_size, double vpeak, 
							   uint scans, uint channels, Moray.Adc adc) {
		string	ts = get_timestamp();
		
		out_stream.puts(@"<seq:sequence timestamp=\"$ts\" xmlns:seq=\"http://apl.uw.edu/oe/seq\">\n");
		out_stream.puts("<seq:metadata>\n");
		out_stream.puts(@"<seq:fsample type=\"l\" units=\"hz\">$fsamp</seq:fsample>\n");
		out_stream.puts(@"<seq:vpeak type=\"f\" units=\"volts\">$vpeak</seq:vpeak>\n");
		out_stream.puts(@"<seq:gain type=\"l\" units=\"dB\">$gain</seq:gain>\n");
		out_stream.puts(@"<seq:sample_size type=\"l\" units=\"bits\">$data_size</seq:sample_size>\n");
		out_stream.printf("<seq:delay type=\"f\" units=\"seconds\">%.6f</seq:delay>\n", delay);
		out_stream.puts("</seq:metadata>\n");
		out_stream.puts("<seq:dimensions>\n");
		out_stream.puts(@"<seq:scans>$scans</seq:scans>\n");
		out_stream.printf("<seq:channels master=\"%u,%u\" slave=\"%u,%u\">%u</seq:channels>\n",
						  adc.master_channels_read, adc.master_mask,
						  adc.slave_channels_read, adc.slave_mask,
						  channels);
		out_stream.printf("<seq:block_size>%u</seq:block_size>\n", adc.scans_per_block);
		out_stream.puts("</seq:dimensions>\n");
	}
	
	static void end_sequence() {
		out_stream.puts("</seq:sequence>\n");
	}

	static void record_data_file(string filename) {
		out_stream.puts(@"<seq:datafile>$filename</seq:datafile>\n");
	}
	
	static void trigger_handler(int state, char[] token) {
		if(state == Moray.TRIG_START) 
		{
			string id = (string)token;
			string	ts = get_timestamp();
			out_stream.puts(@"<seq:trigger timestamp=\"$ts\" id=\"$id\">\n");
		}
		else if(state == Moray.TRIG_END)
			out_stream.puts("</seq:trigger>\n");
	}

	
    static void write_scan(Moray.Scan *sp, uint master_chans, uint master_mask, 
						   uint slave_chans, uint slave_mask, int n_scans) {
		int		i, j;
		uint	mask;
		uint32	*m_buf;
		uint32	*s_buf;

		m_buf = sp->master;
		s_buf = sp->slave;
		
		for(j = 0;j < n_scans;j++) 
		{
			if(pre_acq_delay > 0)
			{
				pre_acq_delay--;
				continue;
			}
			
			out_stream.puts("<seq:scan>");
		
			if(master_mask != 0) {
				for(i = 0,mask = 1;i < master_chans;i++,m_buf++,mask <<= 1) {
					if((mask & master_mask) == mask)
						out_stream.printf("%d ", Moray.DATA_VALUE(*m_buf));
				}
			}

			if(slave_mask != 0) {
				for(i = 0,mask = 1;i < slave_chans;i++,s_buf++,mask <<= 1) {
					if((mask & slave_mask) == mask)
						out_stream.printf("%d ", Moray.DATA_VALUE(*s_buf));
				}
			}

			out_stream.puts("</seq:scan>\n");
		}
		
	}

    static void write_binary_scan(Moray.Scan *sp, uint master_chans, uint master_mask, 
								  uint slave_chans, uint slave_mask, int n_scans) {
		uint32	*m_buf;
		uint32	*s_buf;

		m_buf = sp->master;
		s_buf = sp->slave;
		
		if(pre_acq_delay > 0)
		{
			if(pre_acq_delay > n_scans)
			{
				// Skip this entire block
				pre_acq_delay -= n_scans;
				return;
			}
			// Advance the pointers by pre_acq_delay scans
			m_buf += master_chans*pre_acq_delay;
			s_buf += slave_chans*pre_acq_delay;
			// Adjust the scan count
			n_scans -= pre_acq_delay;
		}
		

		if(master_mask != 0) {
			data_stream.bwrite((void*)m_buf, 4, master_chans*n_scans);
		}

		if(slave_mask != 0) {
			data_stream.bwrite((void*)s_buf, 4, slave_chans*n_scans);
		}

	}

	static void add_attributes(string[] args) {
		string[]	entry;
		
		out_stream.puts("<seq:attributes>\n");
		foreach(string arg in args) 
		{
			entry = arg.split("=", 2);
			out_stream.printf("<%s>%s</%s>\n", entry[0], entry[1], entry[0]);
		}
		out_stream.puts("</seq:attributes>\n");
	}
	
	public static int main(string[] args) {
		string usage = """
On each software trigger, read WINDOW seconds of data from the 12 MORAY A/D
channels (8 from the master, 4 from the slave) at sample rate RATE hz. The
data are streamed to standard output in an XML-encoded format. The optional
ATTR=VALUE arguments can be used to add arbitrary metadata to the output.

Command-line options can be used to set the number of triggers and the interval.
""";

		if (!Thread.supported()) {
			stderr.printf("Cannot run without threads.\n");
			return 1;
		}

		try	{
			var context = new OptionContext("RATE WINDOW [ATTR=VALUE ...]");
			context.set_summary(usage);
			context.add_main_entries(options, null);
			context.set_help_enabled(true);
			context.parse(ref args);
		} catch(OptionError e) 	{
			stderr.printf("%s\n", e.message);
			stderr.printf("Run '%s --help' to see usage message\n", args[0]);
			return 1;
	    }
	
		if(args.length < 3) {
			stderr.puts(usage);
			return 1;
		}

		if(verbose)
			max_level = LogLevelFlags.LEVEL_DEBUG;

		Log.set_default_handler(
			(domain, flags, msg) => {
				uint level = flags & LogLevelFlags.LEVEL_MASK;
				if(level > max_level)
					return;
				Time now = Time();
				now = Time.local(time_t());
				stderr.printf("[%s] %s\n", now.to_string(), msg);
			}
			);
		
		Moray.ScanFunc writer = write_scan;
		if(data_file != null) {
			message("Opening binary output file %s", data_file);
			data_stream = Stdio.BinaryStream.open(data_file, "w");
			writer = write_binary_scan;
		}
		out_stream = stdout;

		uint rate = int.parse(args[1]);
		double window = double.parse(args[2]);
		uint nscans;
		int skip_scans;

		// Sanity check the input voltage range
		if(v_range > 5)
			v_range = 10;
		else if(v_range > 2.5)
			v_range = 5;
		else
			v_range = 2.5;

		// Read the first 8 channels of the master and the first 4 of the slave.
		var adc = new Moray.Adc(Moray.devname(0), (1 << n_master_channels)-1, 
								Moray.devname(1), (1 << n_slave_channels)-1,
			                    scans_per_block);
		rate = (uint)adc.init(rate, 48, 10, (float)v_range);
		nscans = (uint)(window * rate);
		skip_scans = (int)(delay * rate);

		if(skip_scans > 0 && data_file != null) 
		{
			// For binary file output, the pre-acquistion delay must
			// lie on a block boundary.
			skip_scans /= (int)scans_per_block;
			delay = (float)skip_scans/rate;
		}
		
		start_sequence(rate, Moray.DATA_SIZE, v_range, nscans, 
					   n_master_channels+n_slave_channels, adc);
		// Add pre-acquisition delay to the window size
		nscans += skip_scans;

		if(args.length > 3)
			add_attributes(args[3:args.length]);

		if(data_file != null)
			record_data_file(data_file);

		var trig = new Trigger(adc, trig_interval, trig_count);
		
		// Start thread to trigger the ADC
		try {
			Thread.create<void*>(trig.run, false);
		} catch(ThreadError e) {
			error("%s\n", e.message);
		}

		long n_read = 0;
		
		while(trig_count > 0) {
			adc.arm(10*1000);
			pre_acq_delay = skip_scans;
			stderr.puts("Reading samples ...\n");
			n_read = adc.read(nscans, writer, trigger_handler);
			trig_count--;
		}
		
		end_sequence();
		
		return 0;
	}

}
