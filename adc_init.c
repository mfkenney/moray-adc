/*
**
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <fcntl.h>
#include <unistd.h>
#include <24dsi_utils.h>
#include <gsc_utils.h>
#include <glib.h>
#include "moray_adc.h"

#define MASTER_INDEX            "0"
#define SLAVE_INDEX             "1"

#define DEFAULT_READ_TIMEOUT    10

static void
_alloc_scan(scan_t *sp, unsigned master_chans, unsigned slave_chans, unsigned n_scans)
{
    if(master_chans)
    {
        sp->master = (sample_t*)g_malloc0(master_chans*n_scans*sizeof(sample_t));
    }
    else
        sp->master = NULL;

    if(slave_chans)
    {
        sp->slave = (sample_t*)g_malloc0(slave_chans*n_scans*sizeof(sample_t));
    }
    else
        sp->slave = NULL;

}

static void
_free_scan(scan_t *sp)
{
    if(sp->master)
        g_free(sp->master);
    if(sp->slave)
        g_free(sp->slave);
}


/**
 * Open the devices for both A/D boards.
 */
ADC_t*
adc_open(const char *master_dev, unsigned master_mask,
         const char *slave_dev, unsigned slave_mask,
         unsigned n_scans)
{
    ADC_t   *adc;

    adc = malloc(sizeof(ADC_t));
    if(!adc)
        g_error("Cannot allocate memory for ADC_t (errno = %d)", errno);

    memset(adc, 0, sizeof(ADC_t));

    if((adc->master_fd = open(master_dev, O_RDWR)) < 0)
    {
        g_critical("Cannot open %s (errno = %d)", master_dev, errno);
        free(adc);
        return NULL;
    }

    if(slave_dev != NULL && slave_mask > 0 && access(slave_dev, F_OK) == 0)
    {
        if((adc->slave_fd = open(slave_dev, O_RDWR)) < 0)
        {
            g_critical("Cannot open %s (errno = %d)", slave_dev, errno);
            close(adc->master_fd);
            free(adc);
            return NULL;
        }
    }
    else
    {
        g_message("Slave board not used");
        adc->slave_fd = 0;
        slave_mask = 0;
    }

    adc->n_scans = n_scans;
    adc->master_mask = master_mask;
    adc->slave_mask = slave_mask;
    adc->master_channels = adc->slave_channels = 0;

    g_debug("A/D channels selected: %04x, %04x", adc->master_mask, adc->slave_mask);

    if(adc->master_mask & GROUP_0)
        adc->master_channels += 6;
    if(adc->master_mask & GROUP_1)
        adc->master_channels += 6;

    if(adc->slave_mask & GROUP_0)
        adc->slave_channels += 6;
    if(adc->slave_mask & GROUP_1)
        adc->slave_channels += 6;

    g_debug("A/D channels to read: %d, %d", adc->master_channels, adc->slave_channels);

    _alloc_scan(&adc->scan, adc->master_channels, adc->slave_channels, adc->n_scans);

    return adc;
}

unsigned adc_get_nchannels(ADC_t *adc, adc_board_t board)
{
    unsigned rval = 0;

    switch(board)
    {
        case BOARD_MASTER:
            rval = adc->master_channels;
            break;
        case BOARD_SLAVE:
            rval = adc->slave_channels;
            break;
        default:
            break;
    }

    return rval;
}

unsigned adc_get_mask(ADC_t *adc, adc_board_t board)
{
    unsigned rval = 0;

    switch(board)
    {
        case BOARD_MASTER:
            rval = adc->master_mask;
            break;
        case BOARD_SLAVE:
            rval = adc->slave_mask;
            break;
        default:
            break;
    }

    return rval;
}

/*
 * Compute the clock parameters to produce the requested sampling frequency.
 *
 * @param fd  file device for board driver
 * @param fsamp desired sampling frequency in hz
 * @return actual sampling frequency in hz
 */
static long
compute_clock(int fd, adc_clock_t* cp, long fsamp)
{
    cp->fref = -1;
    dsi_fref_compute(fd, 1, 0, &cp->fref);
    cp->fsamp = fsamp;
    dsi_fsamp_validate(fd, -1, 1, &cp->fsamp);
    dsi_fsamp_compute(fd, -1, 1, 0, cp->fref,
                      &cp->fsamp,
                      &cp->nvco,
                      &cp->nref,
                      &cp->nrate,
                      &cp->ndiv);
    return cp->fsamp;
}

static void
_board_set_clock(int fd, unsigned mask, adc_clock_t *cp)
{
    int32_t src;

    if(mask & GROUP_0)
        src = DSI_CH_GRP_SRC_GEN_A;
    else
        src = DSI_CH_GRP_SRC_DISABLE;
    ioctl_set(fd, DSI_IOCTL_CH_GRP_0_SRC, src);

    if(mask & GROUP_1)
        src = DSI_CH_GRP_SRC_GEN_A;
    else
        src = DSI_CH_GRP_SRC_DISABLE;
    ioctl_set(fd, DSI_IOCTL_CH_GRP_1_SRC, src);

    ioctl(fd, DSI_IOCTL_RATE_GEN_A_NVCO, &cp->nvco);
    ioctl(fd, DSI_IOCTL_RATE_GEN_B_NVCO, &cp->nvco);
    ioctl(fd, DSI_IOCTL_RATE_GEN_A_NREF, &cp->nref);
    ioctl(fd, DSI_IOCTL_RATE_GEN_B_NREF, &cp->nref);
    ioctl(fd, DSI_IOCTL_RATE_DIV_0_NDIV, &cp->ndiv);
    ioctl(fd, DSI_IOCTL_RATE_DIV_1_NDIV, &cp->ndiv);
}


static void
_board_init(int fd, long bufsize, long timeout, float v_peak)
{
    ioctl_set(fd, DSI_IOCTL_RX_IO_MODE, GSC_IO_MODE_DMDMA);
    ioctl_set(fd, DSI_IOCTL_RX_IO_OVERFLOW, DSI_IO_OVERFLOW_IGNORE);
    ioctl_set(fd, DSI_IOCTL_RX_IO_UNDERFLOW, DSI_IO_UNDERFLOW_IGNORE);
    ioctl_set(fd, DSI_IOCTL_RX_IO_TIMEOUT, timeout);
    ioctl(fd, DSI_IOCTL_INITIALIZE, NULL);
    ioctl_set(fd, DSI_IOCTL_AIN_MODE, DSI_AIN_MODE_DIFF);
    ioctl_set(fd, DSI_IOCTL_AIN_RANGE, AIN_RANGE_CODE(v_peak));
    ioctl_set(fd, DSI_IOCTL_AIN_BUF_INPUT, DSI_AIN_BUF_INPUT_ENABLE);
    ioctl_set(fd, DSI_IOCTL_AIN_BUF_THRESH, bufsize);
    ioctl_set(fd, DSI_IOCTL_CHANNEL_ORDER, DSI_CHANNEL_ORDER_SYNC);
    ioctl_set(fd, DSI_IOCTL_DATA_FORMAT, DSI_DATA_FORMAT_2S_COMP);
    ioctl_set(fd, DSI_IOCTL_DATA_WIDTH, DATA_WIDTH_CODE());
    ioctl_set(fd, DSI_IOCTL_INIT_MODE, DSI_INIT_MODE_INITIATOR);
    ioctl_set(fd, DSI_IOCTL_EXT_CLK_SRC, DSI_EXT_CLK_SRC_GRP_0);
    ioctl_set(fd, DSI_IOCTL_XCVR_TYPE, DSI_XCVR_TYPE_TTL);
    /* Activate low frequency (40khz) filter */
    dsi_reg_mod(fd, DSI_GSC_BCTLR, LOW_FREQ_FILTER, LOW_FREQ_FILTER);
}

long
adc_init(ADC_t *adc, long fsamp, long bufsize, long timeout, float v_peak)
{

    adc_init_dbus_connection();

    _board_init(adc->master_fd, bufsize, timeout, v_peak);
    adc->v_peak = v_peak;
    if(adc->slave_fd)
        _board_init(adc->slave_fd, bufsize, timeout, v_peak);

    fsamp = compute_clock(adc->master_fd, &adc->clock, fsamp);
    _board_set_clock(adc->master_fd, adc->master_mask, &adc->clock);
    if(adc->slave_fd)
    {
        fsamp = compute_clock(adc->slave_fd, &adc->clock, fsamp);
        _board_set_clock(adc->slave_fd, adc->slave_mask, &adc->clock);
    }

    g_message("Autocalibrating Master A/D board ...");
    ioctl(adc->master_fd, DSI_IOCTL_AUTO_CALIBRATE, NULL);
    g_message("Calibration complete");

    if(adc->slave_fd)
    {
        g_message("Autocalibrating Slave A/D board ...");
        ioctl(adc->slave_fd, DSI_IOCTL_AUTO_CALIBRATE, NULL);
        g_message("Calibration complete");
    }

    g_message("Clearing buffers");

    ioctl(adc->master_fd, DSI_IOCTL_AIN_BUF_CLEAR, NULL);
    if(adc_sync(adc) < 0)
        g_critical("Sync failed");

    if(adc->slave_fd)
    {
        int32_t src;

        ioctl(adc->slave_fd, DSI_IOCTL_AIN_BUF_CLEAR, NULL);

        /*
         * Setup the slave board as the "target" and synchronize with
         * the master.
         */
        ioctl_set(adc->slave_fd, DSI_IOCTL_INIT_MODE, DSI_INIT_MODE_TARGET);
        if(adc->slave_mask & GROUP_0)
            src = DSI_CH_GRP_SRC_DIR_EXTERN;
        else
            src = DSI_CH_GRP_SRC_DISABLE;
        ioctl_set(adc->slave_fd, DSI_IOCTL_CH_GRP_0_SRC, src);

        if(adc->slave_mask & GROUP_1)
            src = DSI_CH_GRP_SRC_DIR_EXTERN;
        else
            src = DSI_CH_GRP_SRC_DISABLE;
        ioctl_set(adc->slave_fd, DSI_IOCTL_CH_GRP_1_SRC, src);
        ioctl_set(adc->slave_fd, DSI_IOCTL_CHANNEL_ORDER, DSI_CHANNEL_ORDER_SYNC);
        g_message("Synchronizing boards");

        if(adc_sync(adc) < 0)
            g_critical("Sync failed");
    }

    return fsamp;
}

void
adc_input_mode(ADC_t *adc, adc_input_mode_t mode)
{
    int32_t   mode_val = DSI_AIN_MODE_DIFF;

    switch(mode)
    {
        case INPUT_DIFF:
            mode_val = DSI_AIN_MODE_DIFF;
            break;
        case INPUT_ZERO:
            mode_val = DSI_AIN_MODE_ZERO;
            break;
        case INPUT_VREF:
            mode_val = DSI_AIN_MODE_ZERO;
            break;
    }

    ioctl_set(adc->master_fd, DSI_IOCTL_AIN_MODE, mode_val);
    if(adc->slave_fd)
        ioctl_set(adc->slave_fd, DSI_IOCTL_AIN_MODE, mode_val);
}

void
adc_vrange(ADC_t *adc, float v_peak)
{
    int   r;

    r = ioctl_set(adc->master_fd, DSI_IOCTL_AIN_RANGE, AIN_RANGE_CODE(v_peak));
    if(r >= 0)
        adc->v_peak = v_peak;

    g_message("Vrange changed autocalibrating Master A/D board ...");
    ioctl(adc->master_fd, DSI_IOCTL_AUTO_CALIBRATE, NULL);
    g_message("Calibration complete");

    if(adc->slave_fd)
    {
        ioctl_set(adc->slave_fd, DSI_IOCTL_AIN_RANGE, AIN_RANGE_CODE(v_peak));
        g_message("Vrange changed autocalibrating Slave A/D board ...");
        ioctl(adc->slave_fd, DSI_IOCTL_AUTO_CALIBRATE, NULL);
        g_message("Calibration complete");
    }
}

void
adc_enable(ADC_t *adc)
{
    ioctl_set(adc->master_fd, DSI_IOCTL_AIN_BUF_INPUT, DSI_AIN_BUF_INPUT_ENABLE);
    if(adc->slave_fd)
        ioctl_set(adc->slave_fd, DSI_IOCTL_AIN_BUF_INPUT, DSI_AIN_BUF_INPUT_ENABLE);
}

void
adc_disable(ADC_t *adc)
{
    ioctl_set(adc->master_fd, DSI_IOCTL_AIN_BUF_INPUT, DSI_AIN_BUF_INPUT_DISABLE);
    if(adc->slave_fd)
        ioctl_set(adc->slave_fd, DSI_IOCTL_AIN_BUF_INPUT, DSI_AIN_BUF_INPUT_DISABLE);
}

void
adc_close(ADC_t *adc)
{
    g_return_if_fail(adc);

    _free_scan(&adc->scan);
    if(adc->slave_fd)
        close(adc->slave_fd);
    close(adc->master_fd);
    free(adc);
}
