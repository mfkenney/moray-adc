namespace Stdio 
{
	[Compact]
	[CCode (cname = "FILE", cheader_filename = "stdio.h")]
	public class BinaryStream : GLib.FileStream {
		[CCode (cname = "fopen")]
		  public static BinaryStream? open(string pathname, string mode);
		[CCode (cname = "fwrite", instance_pos = -1)]
		  public size_t bwrite(void *buf, size_t size, size_t n);
		
	}
}
